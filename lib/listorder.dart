import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ListOrderI extends StatefulWidget {
  ListOrderI({Key? key}) : super(key: key);

  @override
  _ListOrderIState createState() => _ListOrderIState();
}

class _ListOrderIState extends State<ListOrderI> {
  final _formKey = GlobalKey<FormState>();
  String name = 'M';
  int total = 0;
  int price = 0;
  String date = '';

  final _nameController = TextEditingController();
  final _totalController = TextEditingController();
  final _priceController = TextEditingController();
  final _dateController = TextEditingController();

  Future<void> _loadOrder() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      name = prefs.getString('name') ?? 'M';
      _nameController.text = name;
      total = prefs.getInt('total') ?? 0;
      _totalController.text = '$total';
      price = prefs.getInt('price') ?? 0;
      _priceController.text = '$price';
      date = prefs.getString('date') ?? '';
      _dateController.text = date;
    });
  }

  Future<void> _saveOrder() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      prefs.setString('name', name);
      prefs.setInt('total', total);
      prefs.setInt('price', price);
      prefs.setString('date', date);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('List Order'),
          centerTitle: true,
        ),
        body: Form(
          key: _formKey,
          child: Container(
            padding: EdgeInsets.all(36),
            child: ListView(
              children: [
                DropdownButtonFormField(
                  value: name,
                  items: [
                    DropdownMenuItem(
                      child: Row(
                        children: [Text('โออิชิ ชาคูลล์ซ่า ชาเขียวโซดา')],
                      ),
                      value: 'M',
                    ),
                    DropdownMenuItem(
                      child: Row(
                        children: [Text('แฟนต้า น้ำแดง แคน')],
                      ),
                      value: 'P',
                    ),
                    DropdownMenuItem(
                      child: Row(
                        children: [Text('ทวิสโก้')],
                      ),
                      value: 'T',
                    ),
                    DropdownMenuItem(
                      child: Row(
                        children: [Text('วุ้นเส้น ตรามังกรคู่')],
                      ),
                      value: 'V',
                    ),
                    DropdownMenuItem(
                      child: Row(
                        children: [Text('ชาคูลซ่า')],
                      ),
                      value: 'C',
                    ),
                  ],
                  onChanged: (String? newValue) {
                    setState(() {
                      name = newValue!;
                    });
                  },
                ),
                TextFormField(
                  decoration: InputDecoration(labelText: 'จำนวน'),
                ),
                TextFormField(
                  decoration: InputDecoration(labelText: 'ราคา'),
                ),
                TextFormField(
                  decoration: InputDecoration(labelText: 'วันที่'),
                  keyboardType: TextInputType.datetime,
                ),
                Padding(
                  padding: EdgeInsets.only(top: 30),
                  child: ElevatedButton(
                    onPressed: () {
                      _saveOrder();
                    },
                    child: Text('เพิ่ม'),
                    style: ElevatedButton.styleFrom(
                        primary: Colors.blue, padding: EdgeInsets.all(10)),
                  ),
                ),
                Padding(
                    padding: EdgeInsets.only(top: 50),
                    child: Table(
                      border: TableBorder.symmetric(),
                      children: [
                        TableRow(children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                'โออิชิ ชาคูลล์ซ่า ชาเขียวโซดา',
                                style: TextStyle(
                                  fontSize: 16,
                                ),
                              )
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text('2', style: TextStyle(fontSize: 16))
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              Text('600', style: TextStyle(fontSize: 16)),
                              Icon(Icons.delete)
                            ],
                          )
                        ]),
                        TableRow(children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text('แฟนต้า น้ำแดง แคน',
                                  style: TextStyle(fontSize: 16))
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text('3', style: TextStyle(fontSize: 16))
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              Text('600', style: TextStyle(fontSize: 16)),
                              Icon(Icons.delete)
                            ],
                          )
                        ]),
                        TableRow(children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text('วุ้นเส้น ตรามังกรคู่',
                                  style: TextStyle(fontSize: 16))
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text('3', style: TextStyle(fontSize: 16))
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              Text('600', style: TextStyle(fontSize: 16)),
                              Icon(Icons.delete)
                            ],
                          )
                        ]),
                        TableRow(children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                'เลย์สแต็คส์ มันฝรั่งทอดกรอบ รสมันฝรั่งออริจินัล',
                                style: TextStyle(fontSize: 16),
                                textAlign: TextAlign.left,
                              )
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text('3', style: TextStyle(fontSize: 16))
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              Text('600', style: TextStyle(fontSize: 16)),
                              Icon(Icons.delete)
                            ],
                          )
                        ])
                      ],
                    )),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Padding(
                      padding: EdgeInsets.only(right: 55, top: 20),
                      child: Text(
                        'จำนวน     11',
                        style: TextStyle(fontSize: 18),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(right: 30),
                      child: Text('ราคารวม  2,400',
                          style: TextStyle(fontSize: 18)),
                    ),
                  ],
                ),
                Padding(
                  padding: EdgeInsets.all(16),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Padding(
                        padding: EdgeInsets.all(10),
                        child: ElevatedButton(
                          onPressed: () {},
                          child: Text('ยกเลิก'),
                          style: ElevatedButton.styleFrom(primary: Colors.red),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.all(10),
                        child: ElevatedButton(
                          onPressed: () {},
                          child: Text('บันทึก'),
                          style:
                              ElevatedButton.styleFrom(primary: Colors.green),
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ));
  }
}
