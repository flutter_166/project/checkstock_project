import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import 'create_stockin.dart';

class StockIn extends StatefulWidget {
  StockIn({Key? key}) : super(key: key);

  @override
  _StockInState createState() => _StockInState();
}

class _StockInState extends State<StockIn> {
  final Stream<QuerySnapshot> _productStream = FirebaseFirestore.instance
      .collection('products')
      .where(
        'name',
      )
      .snapshots();

  CollectionReference product =
      FirebaseFirestore.instance.collection('products');

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
        stream: _productStream,
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasError) {
            return Text('Something went wrong');
          }

          if (snapshot.connectionState == ConnectionState.waiting) {
            return Text('Loading');
          }

          return Scaffold(
            appBar: AppBar(
              title: Text('Stock In'),
              centerTitle: true,
            ),
            body: ListView(
              children: snapshot.data!.docs.map((DocumentSnapshot document) {
                Map<String, dynamic> data =
                    document.data()! as Map<String, dynamic>;
                return ListTile(
                  title: Text('รหัสสินค้า: ' + data['product_code']),
                  subtitle: Text('ชื่อสินค้า: ' + data['product_name']),
                  trailing: Text(
                    data['total'].toString(),
                    style: TextStyle(fontSize: 18),
                  ),
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                StockInForm(productId: document.id)));
                  },
                );
              }).toList(),
            ),
          );
        });
  }
}
