// import 'dart:ffi';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;

class AddNewProduct extends StatefulWidget {
  String productId;
  AddNewProduct({Key? key, required this.productId}) : super(key: key);

  @override
  _AddNewProductState createState() => _AddNewProductState(this.productId);
}

class _AddNewProductState extends State<AddNewProduct> {
  String? _imageUrl = "";
  String productId;
  String code = "";
  String name = "";
  String price = "0";
  String unit = "";
  String total = "0";
  String minStock = "0";
  String maxStock = "0";
  String insert = "0";
  CollectionReference products =
      FirebaseFirestore.instance.collection('products');
  _AddNewProductState(this.productId);
  TextEditingController _codeController = new TextEditingController();
  TextEditingController _nameController = new TextEditingController();
  TextEditingController _priceController = new TextEditingController();
  TextEditingController _unitController = new TextEditingController();
  TextEditingController _totalController = new TextEditingController();
  TextEditingController _minStockController = new TextEditingController();
  TextEditingController _maxStockController = new TextEditingController();
  TextEditingController _imgController = new TextEditingController();

  @override
  void initState() {
    super.initState();
    if (this.productId.isEmpty) {
      products.doc(this.productId).get().then((snapshot) {
        if (snapshot.exists) {
          var data = snapshot.data() as Map<String, dynamic>;
          code = data['product_code'];
          name = data['product_name'];
          price = data['sale_price'].toString();
          unit = data['unit'];
          total = data['total'].toString();
          minStock = data['minimum_stock'].toString();
          maxStock = data['max_stock'].toString();
          insert = data['insert'].toString();
          _imageUrl = data['image'];
          _codeController.text = code;
          _nameController.text = name;
          _priceController.text = price;
          _unitController.text = unit;
          _totalController.text = total;
          _minStockController.text = minStock;
          _maxStockController.text = maxStock;
          _imgController.text = _imageUrl!;
        }
      });
    }
  }

  final _formkey = GlobalKey<FormState>();

  String _scanBarcode = 'Unknow';

  Future<void> startBarcodeScanStream() async {
    FlutterBarcodeScanner.getBarcodeStreamReceiver(
            '#ff6666', 'Cancel', true, ScanMode.BARCODE)!
        .listen((barcode) => print(barcode));
  }

  Future<void> scanQR() async {
    String barcodeScanRes;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      barcodeScanRes = await FlutterBarcodeScanner.scanBarcode(
          '#ff6666', 'Cancel', true, ScanMode.QR);
      print(barcodeScanRes);
    } on PlatformException {
      barcodeScanRes = 'Failed to get platform version.';
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      _scanBarcode = barcodeScanRes;
    });
  }

  Future<void> scanBarcodeNormal() async {
    String barcodeScanRes;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      barcodeScanRes = await FlutterBarcodeScanner.scanBarcode(
          '#ff6666', 'Cancel', true, ScanMode.BARCODE);
      print(barcodeScanRes);
    } on PlatformException {
      barcodeScanRes = 'Failed to get platform version.';
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      _scanBarcode = barcodeScanRes;
    });
  }

  Future<void> addProduct() {
    return products
        .add({
          'product_code': this.code,
          'product_name': this.name,
          'sale_price': int.parse(price),
          'unit': this.unit,
          'total': int.parse(total),
          'minimum_stock': int.parse(minStock),
          'max_stock': int.parse(maxStock),
          'insert': int.parse(insert),
          'image': this._imageUrl,
        })
        .then((value) => print('Product Added'))
        .catchError((error) => print('Failed to add product: $error'));
  }

  Future<void> updateProduct() {
    return products
        .doc(this.productId)
        .update({
          'product_code': this.code,
          'product_name': this.name,
          'sale_price': int.parse(price),
          'unit': this.unit,
          'total': int.parse(total),
          'minimum_stock': int.parse(minStock),
          'max_stock': int.parse(maxStock),
          'insert': int.parse(insert),
          'image': this._imageUrl,
        })
        .then((value) => print("Product Updated"))
        .catchError((error) => print("Failed to update product: $error"));
  }

  Future<void> _resetProduct() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.remove('code');
    prefs.remove('name');
    prefs.remove('price');
    prefs.remove('unit');
    prefs.remove('total');
    prefs.remove('minStock');
    prefs.remove('maxStock');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Add Product'),
        centerTitle: true,
      ),
      body: Form(
          key: _formkey,
          child: Container(
              padding: EdgeInsets.all(32),
              child: ListView(
                children: [
                  TextFormField(
                    autofocus: true,
                    controller: _codeController,
                    decoration: InputDecoration(
                        labelText: 'รหัสสินค้า',
                        labelStyle: TextStyle(fontSize: 16),
                        icon: IconButton(
                          icon: Icon(Icons.qr_code_scanner),
                          onPressed: () => scanBarcodeNormal(),
                        )),
                    validator: (value) {
                      if (value == null || value.isEmpty || value.length < 13) {
                        return 'กรุณากรอกข้อมูลรหัสสินค้า';
                      } else if (value.length > 13) {
                        return 'กรุณากรอกข้อมูลรหัสสินค้าไม่เกิน 13 ตัว';
                      }
                      return null;
                    },
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    onChanged: (value) {
                      setState(() {
                        code = value;
                        // code = _scanBarcode.toString();
                      });
                    },
                  ),
                  TextFormField(
                    autofocus: true,
                    controller: _nameController,
                    decoration: InputDecoration(
                        labelText: 'ชื่อสินค้า',
                        labelStyle: TextStyle(fontSize: 16)),
                    validator: (value) {
                      if (value == null || value.isEmpty || value.length < 5) {
                        return 'กรุณากรอกข้อมูลชื่อสินค้า';
                      }
                      return null;
                    },
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    onChanged: (value) {
                      setState(() {
                        name = value;
                      });
                    },
                  ),
                  Padding(padding: EdgeInsets.only(top: 10)),
                  TextFormField(
                    autofocus: true,
                    controller: _priceController,
                    decoration: InputDecoration(
                      labelText: 'ราคา',
                      labelStyle: TextStyle(fontSize: 16),
                      border: OutlineInputBorder(),
                    ),
                    keyboardType: TextInputType.number,
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'กรุณากรอกข้อมูลราคา';
                      }
                      return null;
                    },
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    onChanged: (value) {
                      setState(() {
                        price = value;
                      });
                    },
                  ),
                  Padding(padding: EdgeInsets.only(top: 10)),
                  TextFormField(
                    autofocus: true,
                    controller: _unitController,
                    decoration: InputDecoration(
                      labelText: 'หน่วยนับ',
                      labelStyle: TextStyle(fontSize: 16),
                      border: OutlineInputBorder(),
                    ),
                    onChanged: (value) {
                      setState(() {
                        unit = value;
                      });
                    },
                  ),
                  Padding(padding: EdgeInsets.only(top: 10)),
                  TextFormField(
                    autofocus: true,
                    controller: _totalController,
                    decoration: InputDecoration(
                      labelText: 'จำนวน',
                      labelStyle: TextStyle(fontSize: 16),
                      border: OutlineInputBorder(),
                    ),
                    onChanged: (value) {
                      setState(() {
                        total = value;
                      });
                    },
                  ),
                  Padding(padding: EdgeInsets.only(top: 10)),
                  TextFormField(
                    autofocus: true,
                    controller: _minStockController,
                    decoration: InputDecoration(
                      labelText: 'สต็อกขั้นต่ำ',
                      labelStyle: TextStyle(fontSize: 16),
                      border: OutlineInputBorder(),
                    ),
                    keyboardType: TextInputType.number,
                    onChanged: (value) {
                      setState(() {
                        minStock = value;
                      });
                    },
                  ),
                  Padding(padding: EdgeInsets.only(top: 10)),
                  TextFormField(
                    autofocus: true,
                    controller: _maxStockController,
                    decoration: InputDecoration(
                      labelText: 'สต็อกมากสุด',
                      labelStyle: TextStyle(fontSize: 16),
                      border: OutlineInputBorder(),
                    ),
                    keyboardType: TextInputType.number,
                    onChanged: (value) {
                      setState(() {
                        maxStock = value;
                      });
                    },
                  ),
                  Padding(
                    padding: EdgeInsets.all(16),
                    child: TextButton(
                        onPressed: () async {
                          FilePickerResult? result =
                              await FilePicker.platform.pickFiles();
                          if (result != null) {
                            PlatformFile file = result.files.single;
                            await uploadFile(file);
                          } else {
                            Image.network(_imageUrl!);
                          }
                        },
                        child: IconButton(
                            icon: Icon(
                              Icons.photo,
                              color: Colors.cyan,
                            ),
                            tooltip: 'อัพโหลดรูปภาพ',
                            iconSize: 150,
                            onPressed: () {},
                            alignment: AlignmentDirectional.bottomStart)),
                    // Image.network(_imageUrl!)),,
                  ),
                  Padding(padding: EdgeInsets.only(top: 20)),
                  ElevatedButton(
                    child: Text('บันทึก'),
                    onPressed: () async {
                      if (_formkey.currentState!.validate()) {
                        await addProduct();
                        Navigator.pop(context);
                      }
                    },
                    style: ElevatedButton.styleFrom(primary: Colors.green),
                  ),
                  Padding(padding: EdgeInsets.only(top: 20)),
                  ElevatedButton(
                    child: Text('ยกเลิก'),
                    onPressed: () {
                      _resetProduct();
                    },
                    style: ElevatedButton.styleFrom(primary: Colors.red),
                  )
                ],
              ))),
    );
  }

  uploadFile(PlatformFile file) async {
    var uploadTask = await firebase_storage.FirebaseStorage.instance
        .ref('product/${file.name}')
        .putData(file.bytes!);
    String imageUrl = await uploadTask.ref.getDownloadURL();
    setState(() {
      _imageUrl = imageUrl;
    });
  }
}
