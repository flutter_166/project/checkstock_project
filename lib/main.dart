import 'package:checkstock_project/check_list_order.dart';
import 'package:checkstock_project/listorder.dart';
import 'package:checkstock_project/myproduct.dart';
import 'package:checkstock_project/stock_in.dart';
import 'package:checkstock_project/stock_out.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';

void main() {
  runApp(MyApp());
}

class App extends StatefulWidget {
  App({Key? key}) : super(key: key);

  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  final Future<FirebaseApp> _initialization = Firebase.initializeApp();
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: _initialization,
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return Text('Error..');
          } else if (snapshot.connectionState == ConnectionState.done) {
            return MyApp();
          }
          return Text('Loading..');
        });
  }
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Check Stock',
      theme: ThemeData(
        primarySwatch: Colors.cyan,
      ),
      home: MyHomePage(title: 'Main'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  String? _imageUrl;

  // @override
  // void initState() {
  //   super.initState();
  //   setState(() {
  //     _imageUrl = FirebaseAuth.instance.currentUser!.photoURL;
  //   });
  // }

  Future<UserCredential> signInWithGoogle() async {
    // Create a new provider
    GoogleAuthProvider googleProvider = GoogleAuthProvider();

    googleProvider
        .addScope('https://www.googleapis.com/auth/contacts.readonly');
    googleProvider.setCustomParameters({'login_hint': 'user@example.com'});

    // Once signed in, return the UserCredential
    return await FirebaseAuth.instance.signInWithPopup(googleProvider);

    // Or use signInWithRedirect
    // return await FirebaseAuth.instance.signInWithRedirect(googleProvider);
  }

  Future<UserCredential> signInWithGoogleNative() async {
    // Trigger the authentication flow
    final GoogleSignInAccount? googleUser = await GoogleSignIn().signIn();

    // Obtain the auth details from the request
    final GoogleSignInAuthentication googleAuth =
        await googleUser!.authentication;

    // Create a new credential
    final credential = GoogleAuthProvider.credential(
      accessToken: googleAuth.accessToken,
      idToken: googleAuth.idToken,
    );

    // Once signed in, return the UserCredential
    return await FirebaseAuth.instance.signInWithCredential(credential);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: Text(widget.title),
          actions: [
            _imageUrl == null
                ? IconButton(
                    icon: Icon(Icons.login),
                    onPressed: () async {
                      await signInWithGoogle();
                    }, 
                  ) 
                : GestureDetector(
                    child: Image.network(_imageUrl!),
                    onLongPress: () {
                      FirebaseAuth.instance.signOut();
                    },
                  ),
          ],
        ),
        body: GridView.count(
          primary: false,
          padding: EdgeInsets.all(20),
          crossAxisSpacing: 10,
          mainAxisSpacing: 10,
          crossAxisCount: 2,
          children: <Widget>[
            Container(
                padding: EdgeInsets.all(8),
                color: Colors.blueGrey,
                child: TextButton(
                  child: Text(
                    'My Product',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 18,
                    ),
                  ),
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => MyProduct()));
                  },
                )),
            Container(
                padding: EdgeInsets.all(8),
                color: Colors.blueGrey,
                child: TextButton(
                  child: Text(
                    'Stock In',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 18,
                    ),
                  ),
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => StockIn()));
                  },
                )),
            Container(
                padding: EdgeInsets.all(8),
                color: Colors.blueGrey,
                child: TextButton(
                  child: Text(
                    'Stock Out',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 18,
                    ),
                  ),
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => StockOut()));
                  },
                )),
            Container(
                padding: EdgeInsets.all(8),
                color: Colors.blueGrey,
                child: TextButton(
                  child: Text(
                    'List Order',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 18,
                    ),
                  ),
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => ListOrderI()));
                  },
                )),
            Container(
                padding: EdgeInsets.all(8),
                color: Colors.blueGrey,
                child: TextButton(
                  child: Text(
                    'Check List Order',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 18,
                    ),
                  ),
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => CheckListOrder()));
                  },
                ))
          ],
        ));
  }
}
