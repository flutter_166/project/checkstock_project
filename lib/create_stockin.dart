import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class StockInForm extends StatefulWidget {
  String productId;
  StockInForm({Key? key, required this.productId}) : super(key: key);

  @override
  _StockInFormState createState() => _StockInFormState(this.productId);
}

class _StockInFormState extends State<StockInForm> {
  String productId;
  String code = "";
  String name = "";
  String total = "0";
  String insert = "0";
  CollectionReference products =
      FirebaseFirestore.instance.collection('products');
  _StockInFormState(this.productId);
  TextEditingController _codeController = new TextEditingController();
  TextEditingController _nameController = new TextEditingController();
  TextEditingController _totalController = new TextEditingController();
  TextEditingController _insertController = new TextEditingController();

  @override
  void initState() {
    super.initState();
    if (this.productId.isNotEmpty) {
      products.doc(this.productId).get().then((snapshot) {
        if (snapshot.exists) {
          var data = snapshot.data() as Map<String, dynamic>;
          code = data['product_code'];
          name = data['product_name'];
          total = data['total'].toString();
          insert = data['insert'].toString();
          _codeController.text = code;
          _nameController.text = name;
          _totalController.text = total;
          _insertController.text = insert;
        }
      });
    }
  }

  final _formkey = GlobalKey<FormState>();
  Future<void> addProduct() {
    return products
        .add({
          'product_code': this.code,
          'product_name': this.name,
          'total': int.parse(total),
          'insert': int.parse(insert)
        })
        .then((value) => print('Product Added'))
        .catchError((error) => print('Failed to add product: $error'));
  }

  Future<void> updateProduct() {
    return products
        .doc(this.productId)
        .update({
          'product_code': code,
          'product_name': this.name,
          'total': int.parse(total),
          'insert': int.parse(insert)
        })
        .then((value) => print("Product Updated"))
        .catchError((error) => print("Failed to update product: $error"));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Create Stock In'),
          centerTitle: true,
        ),
        body: Padding(
          padding: EdgeInsets.all(16),
          child: Form(
            autovalidateMode: AutovalidateMode.onUserInteraction,
            key: _formkey,
            child: Column(
              children: [
                TextFormField(
                  controller: _codeController,
                  decoration: InputDecoration(labelText: 'รหัสสินค้า'),
                  onChanged: (value) {
                    setState(() {
                      code = value;
                    });
                  },
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'กรุณากรอกรหัสสินค้า';
                    }
                    return null;
                  },
                ),
                TextFormField(
                  controller: _nameController,
                  decoration: InputDecoration(labelText: 'ชื่อสินค้า'),
                  onChanged: (value) {
                    setState(() {
                      name = value;
                    });
                  },
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'กรุณากรอกชื่อสินค้า';
                    }
                    return null;
                  },
                ),
                TextFormField(
                  controller: _totalController,
                  decoration: InputDecoration(labelText: 'จำนวน'),
                  onChanged: (value) {
                    setState(() {
                      total = value;
                    });
                  },
                  validator: (value) {
                    if (value == null ||
                        value.isEmpty ||
                        int.tryParse(value) == null) {
                      return 'กรุณากรอกจำนวน';
                    }
                    return null;
                  },
                ),
                TextFormField(
                    controller: _insertController,
                    decoration: InputDecoration(labelText: 'จำนวนที่เพิ่ม'),
                    onChanged: (value) {
                      setState(() {
                        insert = value;
                      });
                    }),
                Padding(
                  padding: EdgeInsets.only(top: 10),
                  child: ElevatedButton(
                    onPressed: () async {
                      if (_formkey.currentState!.validate()) {
                        if (productId.isEmpty) {
                          await addProduct();
                        } else {
                          await updateProduct();
                        }
                        Navigator.pop(context);
                      }
                    },
                    child: Text('บันทึก'),
                    style: ElevatedButton.styleFrom(primary: Colors.green),
                  ),
                )
              ],
            ),
          ),
        ));
  }
}
