// import 'dart:html';
// import 'dart:ui';

import 'package:checkstock_project/addproduct.dart';
import 'package:checkstock_project/load_detail.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class MyProduct extends StatefulWidget {
  MyProduct({Key? key}) : super(key: key);

  @override
  _MyProductState createState() => _MyProductState();
}

class _MyProductState extends State<MyProduct> {
  final Stream<QuerySnapshot> _productStream =
      FirebaseFirestore.instance.collection('products').snapshots();

  CollectionReference product =
      FirebaseFirestore.instance.collection('products');

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: _productStream,
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (snapshot.hasError) {
          return Text('Something went wrong');
        }

        if (snapshot.connectionState == ConnectionState.waiting) {
          return Text('Loading');
        }

        return Scaffold(
            appBar: AppBar(
              title: Text('My Product'),
              centerTitle: true,
            ),
            body: ListView(
              children: snapshot.data!.docs.map((DocumentSnapshot document) {
                Map<String, dynamic> data =
                    document.data()! as Map<String, dynamic>;
                return ListTile(
                  title: Text('รหัสสินค้า: ' + data['product_code']),
                  subtitle: Text('ชื่อสินค้า: ' + data['product_name']),
                  trailing: Text(
                    data['total'].toString(),
                    style: TextStyle(fontSize: 18),
                  ),
                  leading: Image.asset(
                    'images/Lay.jpg',
                    width: 50,
                    height: 50,
                    fit: BoxFit.cover,
                  ),
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                LoadDetail(productId: document.id)));
                  },
                );
              }).toList(),
            ),
            floatingActionButton: FloatingActionButton(
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => AddNewProduct(
                                productId: product.id,
                              )));
                },
                tooltip: 'Add Product',
                child: Icon(Icons.add)));
      },
    );
  }
}
