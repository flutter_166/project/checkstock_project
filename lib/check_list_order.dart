import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class CheckListOrder extends StatefulWidget {
  CheckListOrder({Key? key}) : super(key: key);

  @override
  _CheckListOrderState createState() => _CheckListOrderState();
}

class _CheckListOrderState extends State<CheckListOrder> {
  final Stream<QuerySnapshot> _productStream = FirebaseFirestore.instance
      .collection('products')
      .where('total', isLessThan: 6)
      .snapshots();

  CollectionReference product =
      FirebaseFirestore.instance.collection('products');

  var productValue = false;

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
        stream: _productStream,
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasError) {
            return Text('Something went wrong');
          }

          if (snapshot.connectionState == ConnectionState.waiting) {
            return Text('Loading');
          }
          return Scaffold(
            appBar: AppBar(
              title: Text('Check List Order'),
              centerTitle: true,
            ),
            body: ListView(
              children: snapshot.data!.docs.map((DocumentSnapshot document) {
                Map<String, dynamic> data =
                    document.data()! as Map<String, dynamic>;
                return CheckboxListTile(
                    controlAffinity: ListTileControlAffinity.leading,
                    title: Text(data['product_name']),
                    secondary: Text(
                      '600',
                      style: TextStyle(fontSize: 20),
                    ),
                    subtitle: Text('จำนวน: 20'),
                    value: productValue,
                    onChanged: (newValue) {
                      setState(() {
                        productValue = newValue!;
                      });
                    });
              }).toList(),
            ),
            floatingActionButton: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Expanded(
                    child: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Padding(
                      padding: EdgeInsets.only(top: 450),
                      child: Text(
                        'ราคารวมทั้งหมด      2,400',
                        style: TextStyle(fontSize: 18),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Padding(
                          padding: EdgeInsets.only(top: 20, right: 10),
                          child: ElevatedButton(
                            child:
                                Text('ยกเลิก', style: TextStyle(fontSize: 18)),
                            onPressed: () {},
                            style:
                                ElevatedButton.styleFrom(primary: Colors.red),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 20),
                          child: ElevatedButton(
                              child: Text(
                                'บันทึก',
                                style: TextStyle(fontSize: 18),
                              ),
                              onPressed: () {},
                              style: ElevatedButton.styleFrom(
                                  primary: Colors.green)),
                        ),
                      ],
                    )
                  ],
                ))
              ],
            ),
          );
        });
  }
}
