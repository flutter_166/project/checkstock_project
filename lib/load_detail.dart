import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class LoadDetail extends StatefulWidget {
  String productId;
  LoadDetail({Key? key, required this.productId}) : super(key: key);

  @override
  _LoadDetailState createState() => _LoadDetailState(this.productId);
}

class _LoadDetailState extends State<LoadDetail> {
  String productId;
  String code = "";
  String name = "";
  String price = "0";
  String unit = "";
  String total = "0";
  String minStock = "0";
  String maxStock = "0";

  CollectionReference products =
      FirebaseFirestore.instance.collection('products');

  _LoadDetailState(this.productId);

  TextEditingController _codeController = new TextEditingController();
  TextEditingController _nameController = new TextEditingController();
  TextEditingController _priceController = new TextEditingController();
  TextEditingController _unitController = new TextEditingController();
  TextEditingController _totalController = new TextEditingController();
  TextEditingController _minStockController = new TextEditingController();
  TextEditingController _maxStockController = new TextEditingController();

  @override
  void initState() {
    super.initState();
    if (this.productId.isEmpty) {
      products.doc(this.productId).get().then((snapshot) {
        if (snapshot.exists) {
          var data = snapshot.data() as Map<String, dynamic>;
          code = data['product_code'];
          name = data['product_name'];
          price = data['sale_price'].toString();
          unit = data['unit'];
          total = data['total'].toString();
          minStock = data['minimum_stock'].toString();
          maxStock = data['max_stock'].toString();
          _codeController.text = code;
          _nameController.text = name;
          _priceController.text = price;
          _unitController.text = unit;
          _totalController.text = total;
          _minStockController.text = minStock;
          _maxStockController.text = maxStock;
        }
      });
    }
  }

  final _formKey = GlobalKey<FormState>();

  Future<void> addProduct() {
    return products
        .add({
          'product_code': this.code,
          'product_name': this.name,
          'sale_price': int.parse(price),
          'unit': this.unit,
          'total': int.parse(total),
          'minimum_stock': int.parse(minStock),
          'max_stock': int.parse(maxStock),
        })
        .then((value) => print('Product Added'))
        .catchError((error) => print('Failed to add product: $error'));
  }

  Future<void> updateProduct() {
    return products
        .doc(this.productId)
        .update({
          'product_code': code,
          'product_name': this.name,
          'sale_price': int.parse(price),
          'unit': unit,
          'total': int.parse(total),
          'minimum_stock': int.parse(minStock),
          'max_stock': int.parse(maxStock)
        })
        .then((value) => print("Product Updated"))
        .catchError((error) => print("Failed to update product: $error"));
  }

  Future<void> delProduct(productId) {
    return products
        .doc(productId)
        .delete()
        .then((value) => print('Product Delete'))
        .catchError((error) => print('Failed to delete product: $error'));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text('Detail'),
        centerTitle: true,
        ),
        
        body: Form(
            autovalidateMode: AutovalidateMode.onUserInteraction,
            key: _formKey,
            child: Container(
                padding: EdgeInsets.all(32),
                child: ListView(
                  children: [
                    Padding(
                        padding: EdgeInsets.all(6),
                        child: Image.asset(
                          'images/Lay.jpg',
                          width: 150,
                          height: 150,
                          fit: BoxFit.fitHeight,
                        )),
                    TextFormField(
                      controller: _codeController,
                      decoration: InputDecoration(
                          labelText: 'รหัสสินค้า',
                          labelStyle: TextStyle(fontSize: 16)),
                      enableSuggestions: true,
                      onChanged: (value) {
                        setState(() {
                          code = value;
                        });
                      },
                      validator: (value) {
                        if (value == null ||
                            value.isEmpty ||
                            value.length < 13) {
                          return 'กรุณากรอกข้อมูลรหัสสินค้า';
                        } else if (value.length > 13) {
                          return 'กรุณากรอกข้อมูลรหัสสินค้าไม่เกิน 13 ตัว';
                        }
                        return null;
                      },
                    ),
                    TextFormField(
                      controller: _nameController,
                      decoration: InputDecoration(
                          labelText: 'ชื่อสินค้า',
                          labelStyle: TextStyle(fontSize: 16)),
                      onChanged: (value) {
                        setState(() {
                          name = value;
                        });
                      },
                      validator: (value) {
                        if (value == null ||
                            value.isEmpty ||
                            value.length < 5) {
                          return 'กรุณากรอกข้อมูลชื่อสินค้า';
                        }
                        return null;
                      },
                    ),
                    Padding(padding: EdgeInsets.only(top: 10)),
                    TextFormField(
                      controller: _priceController,
                      decoration: InputDecoration(
                        labelText: 'ราคา',
                        labelStyle: TextStyle(fontSize: 16),
                        border: OutlineInputBorder(),
                      ),
                      keyboardType: TextInputType.number,
                      onChanged: (value) {
                        setState(() {
                          price = value;
                        });
                      },
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'กรุณากรอกข้อมูลราคา';
                        }
                        return null;
                      },
                    ),
                    Padding(padding: EdgeInsets.only(top: 10)),
                    TextFormField(
                      controller: _unitController,
                      decoration: InputDecoration(
                        labelText: 'หน่วยนับ',
                        labelStyle: TextStyle(fontSize: 16),
                        border: OutlineInputBorder(),
                      ),
                      onChanged: (value) {
                        setState(() {
                          unit = value;
                        });
                      },
                    ),
                    Padding(padding: EdgeInsets.only(top: 10)),
                    TextFormField(
                      controller: _totalController,
                      decoration: InputDecoration(
                        labelText: 'จำนวน',
                        labelStyle: TextStyle(fontSize: 16),
                        border: OutlineInputBorder(),
                      ),
                      onChanged: (value) {
                        setState(() {
                          total = value;
                        });
                      },
                    ),
                    Padding(padding: EdgeInsets.only(top: 10)),
                    TextFormField(
                      controller: _minStockController,
                      decoration: InputDecoration(
                        labelText: 'สต็อกขั้นต่ำ',
                        labelStyle: TextStyle(fontSize: 16),
                        border: OutlineInputBorder(),
                      ),
                      keyboardType: TextInputType.number,
                      onChanged: (value) {
                        setState(() {
                          minStock = value;
                        });
                      },
                    ),
                    Padding(padding: EdgeInsets.only(top: 10)),
                    TextFormField(
                      controller: _maxStockController,
                      decoration: InputDecoration(
                        labelText: 'สต็อกมากสุด',
                        labelStyle: TextStyle(fontSize: 16),
                        border: OutlineInputBorder(),
                      ),
                      keyboardType: TextInputType.number,
                      onChanged: (value) {
                        setState(() {
                          maxStock = value;
                        });
                      },
                    ),
                    Padding(padding: EdgeInsets.only(top: 20)),
                    ElevatedButton(
                      child: Text('บันทึก'),
                      onPressed: () async {
                        if (_formKey.currentState!.validate()) {
                          await updateProduct();
                          Navigator.pop(context);
                        }
                      },
                      style: ElevatedButton.styleFrom(primary: Colors.green),
                    ),
                    Padding(padding: EdgeInsets.only(top: 20)),
                    ElevatedButton(
                      child: Text('ลบ'),
                      onPressed: () async {
                        await delProduct(productId);
                        Navigator.pop(context);
                      },
                      style: ElevatedButton.styleFrom(primary: Colors.red),
                    )
                  ],
                ))));
  }
}
